# Demo #
Demo Java dengan [AdminFaces Starter](https://adminfaces.github.io/site/).
Simple project dari db, orm sampai interface

### Bahan ###
* JDK : jdk-8u181-windows-x64.exe
* Netbeans 8.2,  versi terakhir yang mendukung java 8
* Dbeaver atau yang lain, bebas untuk akses database
* Enhanced class redefinition for Java (sunah)
    ** Dcevm : DCEVM-8u181-installer.jar
    ** Hotswab agent : hotswap-agent-1.4.1.jar
* Web server : Wildfly wildfly-21.0.2.Final
* Maven 3.6.3: untuk menggantikan maven bawaan netbeans 8.2
* MariaDB
* Library(setting di maven, gak perlu download manual)
    ** ambil admin starter : Primefaces, Admin LTE
    ** Mysql jdbc
    ** Mybatis
    ** Primefaces
    ** Omnifaces


### Running ###

It should run in any JavaEE 6 or greater application server.

You can also run via wildfly-swarm with command `mvn wildfly-swarm:run -Pswarm`.


The application is available at http://localhost:8080/demo_java

