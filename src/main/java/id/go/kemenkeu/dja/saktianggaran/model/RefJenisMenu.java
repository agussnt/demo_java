/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.go.kemenkeu.dja.saktianggaran.model;

import java.io.Serializable;
import java.util.Objects;


public class RefJenisMenu implements Serializable{
    private Integer id;
    private String kode;
    private String nama;
    
  public RefJenisMenu() {

    }  
    
    public RefJenisMenu(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RefJenisMenu other = (RefJenisMenu) obj;
        if (!Objects.equals(this.kode, other.kode)) {
            return false;
        }
        return true;
    }
    
    
    
}
