package id.go.kemenkeu.dja.saktianggaran.mapper;

import id.go.kemenkeu.dja.saktianggaran.model.RefJenisMenu;
import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

public interface AdminMapper2 {

    @Select("select id,kode,nama from m_ref_jenis_menu where id = #{id} ")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "kode", column = "kode"),
        @Result(property = "nama", column = "nama"),})
    public RefJenisMenu getRefJenisMenu(@Param("ïd") Long id);
    
    @Select("select id,kode,nama from m_ref_jenis_menu order by kode")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "kode", column = "kode"),
        @Result(property = "nama", column = "nama"),})
    public List<RefJenisMenu> getRefJenisMenus();
    
    @Insert("insert into m_ref_jenis_menu(kode,nama) values (#{data.kode},#{data.nama})" )
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public Integer rekamRefJenisMenu(@Param("data") RefJenisMenu data);
    
    @Select("update m_ref_jenis_menu set kode=#{data.kode}, nama=#{data.nama} where id=#{data.id}")
    public void ubahRefJenisMenu(@Param("data") RefJenisMenu data);
    
    @Select("delete from m_ref_jenis_menu where id=#{id}")
    public void hapusRefJenisMenu(@Param("id") Integer id);
}
