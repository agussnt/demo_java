package id.go.kemenkeu.dja.saktianggaran;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Stateless
public class CetakMB implements Serializable {

    Collection<Map<String, ?>> datanya = new ArrayList<>();
    HttpServletResponse httpServletResponse;
    ServletOutputStream servletOutputStream;
    JRExporter exporter = null;

    
    public void cetakBeanCollection(String reportPath, Collection data, String outputType, Map param) throws IOException, JRException, ServletException {
        JRBeanCollectionDataSource mapCol = new JRBeanCollectionDataSource(data);
        JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, param, mapCol);
        jalankanCetak(reportPath, jasperPrint, outputType);
    }

    public void cetakMapCollection(String reportPath, Collection data, String outputType, Map param) throws IOException, JRException, ServletException {
        JRMapCollectionDataSource mapCol = new JRMapCollectionDataSource(data);
        JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, param, mapCol);
        jalankanCetak(reportPath, jasperPrint, outputType);
    }

    public void cetakMapArray(String reportPath, Object[] data, String outputType, Map param) throws IOException, JRException, ServletException {
        JRMapArrayDataSource mapCol = new JRMapArrayDataSource(data);
        JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, param, mapCol);
        jalankanCetak(reportPath, jasperPrint, outputType);
    }

    public void cetakConnection(String reportPath, Connection con, String outputType, Map param) throws IOException, JRException, ServletException {
        JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, param, con);
        jalankanCetak(reportPath, jasperPrint, outputType);
    }


    public void jalankanCetak(String reportPath, JasperPrint jasperPrint, String outputType) throws IOException, JRException, ServletException {
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        servletOutputStream = httpServletResponse.getOutputStream();

        if ("pdf".equalsIgnoreCase(outputType)) {
            //    servletOutputStream = httpServletResponse.getOutputStream();
            httpServletResponse.setContentType("application/pdf");
            httpServletResponse.addHeader("Content-Disposition", "inline; filename=\"file.pdf\"");
            exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        } else if ("rtf".equalsIgnoreCase(outputType)) {
            httpServletResponse.setContentType("application/rtf");
            httpServletResponse.setHeader("Content-Disposition", "inline; filename=\"file.rtf\"");
            exporter = new JRRtfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        } else if ("xls".equalsIgnoreCase(outputType)) { // transfer Excel
            httpServletResponse.setContentType("application/xls");
            httpServletResponse.setHeader("Content-Disposition", "inline; filename=\"file.xls\"");
            exporter = new JRXlsExporter();
            exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BACKGROUND, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
            // exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,
            // Boolean.TRUE);
            // exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
            // Boolean.TRUE);
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        } else if ("csv".equalsIgnoreCase(outputType)) { // transfer csv
            httpServletResponse.setContentType("application/csv");
            httpServletResponse.setHeader("Content-Disposition", "inline; filename=\"file.csv\"");
            exporter = new JRCsvExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            //          exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        } else if ("xlsx".equalsIgnoreCase(outputType)) { // transfer Excelx
            httpServletResponse.setContentType("application/xlsx");
            httpServletResponse.setHeader("Content-Disposition", "inline; filename=\"file.xlsx\"");
            exporter = new JRXlsxExporter();
            exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BACKGROUND, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
            // exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,
            // Boolean.TRUE);
            // exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
            // Boolean.TRUE);
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        } else if ("odt".equalsIgnoreCase(outputType)) { // transfer Excell odt
            httpServletResponse.setContentType("application/odt");
            httpServletResponse.setHeader("Content-Disposition", "inline; filename=\"file.odt\"");
            exporter = new JROdtExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        } else if ("ods".equalsIgnoreCase(outputType)) { // transfer Excell odt
            httpServletResponse.setContentType("application/ods");
            httpServletResponse.setHeader("Content-Disposition", "inline; filename=\"file.ods\"");
            exporter = new JROdsExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            //          exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        } else if ("pptx".equalsIgnoreCase(outputType)) {
            httpServletResponse.setContentType("application/pptx");
            httpServletResponse.addHeader("Content-Disposition", "inline; filename=\"file.pptx\"");
            exporter = new JRPptxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        } else if ("docx".equalsIgnoreCase(outputType)) {
            httpServletResponse.setContentType("application/docx");
            httpServletResponse.addHeader("Content-Disposition", "inline; filename=\"file.docx\"");
            exporter = new JRDocxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        }
        try {
            exporter.exportReport();
            FacesContext.getCurrentInstance().responseComplete();
            FacesContext.getCurrentInstance().renderResponse();
        } catch (JRException e) {
            throw new ServletException(e);
        } finally {
//            if (servletOutputStream != null) {
//                try {
//                    servletOutputStream.close();
//                } catch (IOException ex) {
//                }
//            }
        }
    }
}
