/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.go.kemenkeu.dja.saktianggaran.mapper;

import id.go.kemenkeu.dja.saktianggaran.model.RefJenisMenu;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 *
 * @author agus.sunarto
 */
public interface AdminMapper {

    @Select("SELECT id, kode, nama \n"
            + "	FROM m_ref_jenis_menu WHERE id = #{id} ")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "kode", column = "kode"),
        @Result(property = "nama", column = "nama")
    })
    public RefJenisMenu getRefJenisMenu(@Param("id") Long id);

    @Select("SELECT id, kode, nama \n"
            + "	FROM m_ref_jenis_menu order by kode")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "kode", column = "kode"),
        @Result(property = "nama", column = "nama")
    })
    public List<RefJenisMenu> getRefJenisMenus();

    @Insert("INSERT INTO m_ref_jenis_menu"
            + "(kode, nama) VALUES(#{data.kode}, #{data.nama})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public int insertRefJenismenu(@Param("data") RefJenisMenu data);

    @Select("update m_ref_jenis_menu set kode=#{data.kode}, nama=#{data.nama}"
            + " WHERE id = #{data.id}")
    public void updateRefJenisMenu(@Param("data") RefJenisMenu data);

    @Select("delete from m_ref_jenis_menu "
            + " WHERE id = #{id}")
    public void deleteRefJenisMenu(@Param("id") Integer id);
    
    final String SQL = "${sql}";
    @Select(SQL)
    public List<Map<String,Object>> getDenganSqlCustom(HashMap<String, String> m);
}
