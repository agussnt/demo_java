/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.go.kemenkeu.dja.saktianggaran.service;

import id.go.kemenkeu.dja.saktianggaran.dao.AdminDao;
import id.go.kemenkeu.dja.saktianggaran.mapper.AdminMapper;
import id.go.kemenkeu.dja.saktianggaran.model.RefJenisMenu;
import id.go.kemenkeu.dja.saktianggaran.service.util.MyBatisConnectionFactory;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 *
 * @author agus.sunarto
 */
@Stateless
public class AdminService implements Serializable {

    private SqlSessionFactory sqlSessionFactory;
    private SqlSession session = null;
    AdminDao dao = new AdminDao();

    @PostConstruct
    public void init() {
        try {
            sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public RefJenisMenu getRefJenisMenu(Long id) throws Exception {
        try {
            session = sqlSessionFactory.openSession();
            return dao.getRefJenisMenu(id, session);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public List<RefJenisMenu> getRefJenisMenus() throws Exception {
        try {
            session = sqlSessionFactory.openSession();
            return dao.getRefJenisMenus(session);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public RefJenisMenu insertRefJenismenu(RefJenisMenu data) throws Exception {
        try {
            session = sqlSessionFactory.openSession(false);
            RefJenisMenu hasil = dao.insertRefJenismenu(data, session);
            session.commit();
            return hasil;
        } catch (Exception e) {
            session.rollback();
            throw new Exception(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void updateRefJenisMenu(RefJenisMenu data) throws Exception {
        try {
            session = sqlSessionFactory.openSession(false);
            dao.updateRefJenisMenu(data, session);
            session.commit();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void deleteRefJenisMenu(Integer id) throws Exception {
        try {
            session = sqlSessionFactory.openSession(false);
            dao.deleteRefJenisMenu(id, session);
            session.commit();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
