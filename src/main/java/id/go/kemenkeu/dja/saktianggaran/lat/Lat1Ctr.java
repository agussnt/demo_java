/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.go.kemenkeu.dja.saktianggaran.lat;

import com.github.adminfaces.starter.model.Car;
import com.github.adminfaces.template.exception.BusinessException;
import static com.github.adminfaces.template.util.Assert.has;
import id.go.kemenkeu.dja.saktianggaran.CetakMB;
import id.go.kemenkeu.dja.saktianggaran.mapper.AdminMapper2;
import id.go.kemenkeu.dja.saktianggaran.model.RefJenisMenu;
import id.go.kemenkeu.dja.saktianggaran.service.util.MyBatisConnectionFactory;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import net.sf.jasperreports.engine.JRException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import org.omnifaces.cdi.ViewScoped;
import org.primefaces.PrimeFaces;

/**
 *
 * @author agus.sunarto
 */
@Named("lat1Ctr")
@SessionScoped
public class Lat1Ctr implements Serializable {

    RefJenisMenu refJenisMenu = new RefJenisMenu();
    List<RefJenisMenu> refJenisMenus = new ArrayList<>();
    SqlSessionFactory sqlSessionFactory;
    String statusOperasi = ""; // status rekam atau ubah
    @Inject
    CetakMB cetakMB;

    @PostConstruct
    public void init() {
        sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        loadData();
    }

    public void clearData() {
        refJenisMenus.clear();
    }

    public void loadData() {
        refJenisMenus.clear();
        RefJenisMenu ref1 = new RefJenisMenu();
        ref1.setId(1);
        ref1.setKode("01");
        ref1.setNama("Jenis 01");
        refJenisMenus.add(ref1);

        RefJenisMenu ref2 = new RefJenisMenu();
        ref2.setKode("02");
        ref2.setNama("Jenis 02");
        ref2.setId(2);
        refJenisMenus.add(ref2);

        RefJenisMenu ref3 = new RefJenisMenu("03", "Jenis 03");
        ref3.setId(3);
        refJenisMenus.add(ref3);

//        refJenisMenus.add(new RefJenisMenu("04", "Jenis 04"));
    }

    public void loadDataDb() {
        try {
            SqlSession session = sqlSessionFactory.openSession();
            AdminMapper2 mapper = session.getMapper(AdminMapper2.class);
            refJenisMenus = mapper.getRefJenisMenus();
            session.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal mengambil data Jenis Menu. " + ex.getMessage()));
        }
    }

    public void rekamDataDb() {
        try {
            SqlSession session = sqlSessionFactory.openSession(false);
            AdminMapper2 mapper = session.getMapper(AdminMapper2.class);
            Integer idnya = mapper.rekamRefJenisMenu(refJenisMenu);
            refJenisMenu.setId(idnya);
            session.commit();
            session.close();
            loadDataDb();

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal simpan data Jenis Menu. " + ex.getMessage()));
        }
    }

    public void simpan() {
        if (statusOperasi.equals("Rekam Data")) {
            rekamDataDb();
        } else {
            ubahDataDb();
        }
        PrimeFaces pf = PrimeFaces.current();
        pf.executeScript("PF('addJenisMenuDialog').hide()");
    }

    public void ubahDataDb() {
        try {
            SqlSession session = sqlSessionFactory.openSession(false);
            AdminMapper2 mapper = session.getMapper(AdminMapper2.class);
            mapper.ubahRefJenisMenu(refJenisMenu);
            session.commit();
            session.close();
            loadDataDb();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal ubah data Jenis Menu. " + ex.getMessage()));
        }
    }

    public RefJenisMenu hapusDataDb(RefJenisMenu data) {
        try {
            SqlSession session = sqlSessionFactory.openSession(false);
            AdminMapper2 mapper = session.getMapper(AdminMapper2.class);
            mapper.hapusRefJenisMenu(data.getId());
            session.commit();
            session.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan", "Gagal hapus data Jenis Menu. " + ex.getMessage()));
        }
        return data;
    }

    public void onRekam() {
        statusOperasi = "Rekam Data";
        refJenisMenu = new RefJenisMenu();
        PrimeFaces pf = PrimeFaces.current();
        pf.executeScript("PF('addJenisMenuDialog').show()");
    }

    public void onUbah() {
        statusOperasi = "Ubah Data";
        PrimeFaces pf = PrimeFaces.current();
        pf.executeScript("PF('addJenisMenuDialog').show()");
    }

    public void cetak(String formatOutput) {
        try {
            String jaspernya = "jenis_menu_A4.jasper";
            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            String reportPath = ctx.getRealPath("report") + File.separator + jaspernya;
            Map param = new HashMap();
            param.put("judul", "DAFTAR JENIS MENU");
//            cetakMB.cetakBeanCollection(reportPath, refJenisMenus, formatOutput, param);
            
            SqlSession session = sqlSessionFactory.openSession(false);
            cetakMB.cetakConnection(reportPath, session.getConnection(), jaspernya, param);
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan", "Gagal Cetak " + ex.getMessage()));
        }
    }

    public void onKodeChange() {
        if (refJenisMenu.getKode().equals("aa")) {
            FacesContext.getCurrentInstance().addMessage("frmInput:txtKode",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kode sudah ada", "Kode sudah ada"));
            PrimeFaces pf = PrimeFaces.current();
            pf.executeScript("PF('widgetSimpan').disable()");
        }else{
             PrimeFaces pf = PrimeFaces.current();
            pf.executeScript("PF('widgetSimpan').enable()");
        }
    }

    public RefJenisMenu getRefJenisMenu() {
        return refJenisMenu;
    }

    public void setRefJenisMenu(RefJenisMenu refJenisMenu) {
        this.refJenisMenu = refJenisMenu;
    }

    public List<RefJenisMenu> getRefJenisMenus() {
        return refJenisMenus;
    }

    public void setRefJenisMenus(List<RefJenisMenu> refJenisMenus) {
        this.refJenisMenus = refJenisMenus;
    }

    public String getStatusOperasi() {
        return statusOperasi;
    }

}
