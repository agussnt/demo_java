/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.go.kemenkeu.dja.saktianggaran.service.util;

import id.go.kemenkeu.dja.saktianggaran.mapper.AdminMapper;
import id.go.kemenkeu.dja.saktianggaran.mapper.AdminMapper2;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

/**
 *
 * @author agus.sunarto
 */
public class MyBatisConnectionFactory {

    private static SqlSessionFactory sqlSessionFactory;

    public MyBatisConnectionFactory() {

    }

    static {
        try {
            if (sqlSessionFactory == null) {
                Properties pr = loadProperties("saktianggaran.properties");
                String user = "root";
                String password = "rootjuga";
                String dbServer = pr.getProperty("dbServer");
                String dbName = pr.getProperty("dbName");
                String dbPort = pr.getProperty("dbPort");
                String dbDriver = pr.getProperty("dbDriver");
                String databasenameURL = "";

                databasenameURL = String.format(formatKoneksi(dbDriver), dbServer, dbPort, dbName);
                System.out.println("databasenameURL :" + databasenameURL);
                DataSource dataSource = new org.apache.ibatis.datasource.pooled.PooledDataSource(
                        dbDriver, databasenameURL, user, password);
                TransactionFactory transactionFactory = new JdbcTransactionFactory();
                Environment environment = new Environment("development",
                        transactionFactory, dataSource);
                Configuration configuration = new Configuration(environment);
                configuration.addMapper(AdminMapper.class);
                configuration.addMapper(AdminMapper2.class);
                sqlSessionFactory = new SqlSessionFactoryBuilder()
                        .build(configuration);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            try {
                throw ex;
            } catch (Exception ex1) {
                Logger.getLogger(MyBatisConnectionFactory.class.getName()).log(Level.SEVERE, null, ex1);
            }

        }
    }

    static Properties loadProperties(String propertiesFile) throws Exception {
        String folderProp = "";
        folderProp = System.getProperty("jboss.server.config.dir") + File.separator + "DJA";
        String fileName = folderProp + File.separator + propertiesFile;
        File ff = new File(fileName);
        System.out.println("filename :" + fileName);
        if (!ff.exists()) {
            throw new Exception("The configuration " + propertiesFile + " could not be found: ");
        } else {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(fileName);
                Properties props = new Properties();
                props.load(fis);
                return props;
            } catch (IOException ex) {
                Logger.getLogger(MyBatisConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
                throw new Exception(ex.getMessage());
            } finally {
                if (fis != null) {
                    fis.close();
                }
            }
        }
    }

    static String formatKoneksi(String jenisdb) {
        /**
         * org.postgresql.Driver || jdbc:postgresql://{host}:{port}/{database}
         * com.mysql.jdbc.Driver || jdbc:mysql://{host}:{port}/{database}
         * net.sourceforge.jtds.jdbc.Driver ||
         * jdbc:sqlserver://{host}:{port}/;[databaseName={database}]
         * com.microsoft.sqlserver.jdbc.SQLServerDriver ||
         * jdbc:jtds:sqlserver://{host}:{port}/{database}
         * org.firebirdsql.jdbc.FBDriver ||
         * jdbc:firebirdsql://{host}:{port}/{file} sun.jdbc.odbc.JdbcOdbcDriver
         * || jdbc:odbc:mydsn
         */
        Map<String, String> format = new HashMap();
        format.put("org.postgresql.Driver", "jdbc:postgresql://%s:%s/%s");
        format.put("com.mysql.jdbc.Driver", "jdbc:mysql://%s:%s/%s");
        return format.get(jenisdb);
    }

    public static SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }
}
