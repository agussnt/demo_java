/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.go.kemenkeu.dja.saktianggaran.dao;

import id.go.kemenkeu.dja.saktianggaran.mapper.AdminMapper;
import id.go.kemenkeu.dja.saktianggaran.model.RefJenisMenu;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author agus.sunarto
 */
public class AdminDao {

    public RefJenisMenu getRefJenisMenu(Long id, SqlSession session) throws Exception {
        try {
            AdminMapper mapper = session.getMapper(AdminMapper.class);
            return mapper.getRefJenisMenu(id);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public List<RefJenisMenu> getRefJenisMenus(SqlSession session) throws Exception {

        try {
            AdminMapper mapper = session.getMapper(AdminMapper.class);
            return mapper.getRefJenisMenus();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public RefJenisMenu insertRefJenismenu(RefJenisMenu data, SqlSession session) throws Exception {
        try {
            AdminMapper mapper = session.getMapper(AdminMapper.class);
            data.setId(mapper.insertRefJenismenu(data));
            return data;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void updateRefJenisMenu(RefJenisMenu data, SqlSession session) throws Exception {
        try {
            AdminMapper mapper = session.getMapper(AdminMapper.class);
            mapper.updateRefJenisMenu(data);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void deleteRefJenisMenu(Integer id, SqlSession session) throws Exception {
        try {
            AdminMapper mapper = session.getMapper(AdminMapper.class);
            mapper.deleteRefJenisMenu(id);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public List<Map<String, Object>> getDenganSqlCustom(String sqlnya, SqlSession session) throws Exception {
        try {
            HashMap<String, String> m = new HashMap<>();
            m.put("sql", sqlnya);
            AdminMapper mapper = session.getMapper(AdminMapper.class);
            return mapper.getDenganSqlCustom(m);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
