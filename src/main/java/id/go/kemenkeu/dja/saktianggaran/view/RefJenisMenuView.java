/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.go.kemenkeu.dja.saktianggaran.view;

import id.go.kemenkeu.dja.saktianggaran.model.RefJenisMenu;
import id.go.kemenkeu.dja.saktianggaran.service.AdminService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.PrimeFaces;

/**
 *
 * @author agus.sunarto
 */
@Named
@ViewScoped
public class RefJenisMenuView implements Serializable {

    private List<RefJenisMenu> refJenisMenus = new ArrayList();
    private RefJenisMenu refJenisMenuSelected = new RefJenisMenu();
//    private boolean btnAddDisable = false, btnEditDisable = true;
    private String dialogHeader;
    int mode;
    
    @Inject
    AdminService adminService;

    @PostConstruct
    void init() {
        loadData();
    }

    void loadData() {
        try {
            refJenisMenus = adminService.getRefJenisMenus();
            System.out.println("Jumlah ref jenis menu : " + refJenisMenus.size());
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal mengambil data Jenis Menu. " + ex.toString()));
            Logger.getLogger(RefJenisMenuView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowSelected() {
//        btnEditDisable = refJenisMenuSelected.getId() == null;
    }

    public void onAdd() {
        dialogHeader = "Tambah Data Jenis Menuu";
        mode = 1;
        refJenisMenuSelected = new RefJenisMenu();
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Ngapain mau nambah?" ));
        PrimeFaces pf = PrimeFaces.current();
        pf.executeScript("PF('addJenisMenuDialog').show()");
    }

    public void onEdit() {
        dialogHeader = "Ubah Data Jenis Menu";
        mode = 2;
        PrimeFaces pf = PrimeFaces.current();
        pf.executeScript("PF('addJenisMenuDialog').show()");
    }

    public void onDelete() {
        try {
            adminService.deleteRefJenisMenu(refJenisMenuSelected.getId());
            loadData();
            PrimeFaces pf = PrimeFaces.current();
            pf.executeScript("PF('dlgDelJenisMenu').hide()");
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    ex.toString()));
            Logger.getLogger(RefJenisMenuView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onSave() {
        try {
            switch (mode) {
                case 1: {
                    adminService.insertRefJenismenu(refJenisMenuSelected);
                }
                break;
                case 2:
                    adminService.updateRefJenisMenu(refJenisMenuSelected);
                    break;
            }
            loadData();
            PrimeFaces pf = PrimeFaces.current();
            pf.executeScript("PF('addJenisMenuDialog').hide()");
        } catch (Exception ex) {
            Logger.getLogger(RefJenisMenuView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

        public List<RefJenisMenu> getRefJenisMenus() {
        return refJenisMenus;
    }

    public void setRefJenisMenus(List<RefJenisMenu> refJenisMenus) {
        this.refJenisMenus = refJenisMenus;
    }

    public RefJenisMenu getRefJenisMenuSelected() {
        return refJenisMenuSelected;
    }

    public void setRefJenisMenuSelected(RefJenisMenu refJenisMenuSelected) {
        this.refJenisMenuSelected = refJenisMenuSelected;
    }

 
    public String getDialogHeader() {
        return dialogHeader;
    }

    public void setDialogHeader(String dialogHeader) {
        this.dialogHeader = dialogHeader;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

}
